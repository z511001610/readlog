/**
 * 
 */
package pers.vic.readlog.log.model;

/**
 * @author Vic.xu
 *
 */
public class MsgState {
	/**
	 * 0 - 正常传输
	 * 1 - 暂停
	 * -1 - 未开始
	 */
	private int state = -1;
	
	private String  dir;
	
	private String fileName;

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
	

}
