/**
 * 
 */
package pers.vic.readlog.log.model;

import java.io.File;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * @author Vic.xu
 *
 */
public class LogModel {

	private String fileName;


	private Long size;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateDate;
	
	public LogModel() {}
	
	public LogModel(File file){
		if(file != null && file.exists()) {
			this.fileName = file.getName();
			this.size = file.length();
			this.updateDate = new Date(file.lastModified());
		}
	}
	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getSizeDesc() {
		if (size < 1024) {
			return size + "b";
		}
		if (size < 1024 * 1024) {
			return size / 1024 + "k";
		}
		return size / 1024 / 1024 + "m";
	}

}
