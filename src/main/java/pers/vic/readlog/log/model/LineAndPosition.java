/**
 * 
 */
package pers.vic.readlog.log.model;

/**
 * 
 * 记录读取日志的位置和行数
 * @author Vic.xu
 *
 */
public class LineAndPosition {

	private int line;
	
	private long position;
	
	public LineAndPosition() {
		super();
	}

	public LineAndPosition(int line, long position) {
		super();
		this.line = line;
		this.position = position;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public long getPosition() {
		return position;
	}

	public void setPosition(long position) {
		this.position = position;
	}
	
	
}
