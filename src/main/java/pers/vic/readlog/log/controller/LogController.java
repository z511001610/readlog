/**
 * 
 */
package pers.vic.readlog.log.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.readlog.constant.ReadlogConstant;
import pers.vic.readlog.log.model.LogModel;
import pers.vic.readlog.log.service.LogService;
import pers.vic.readlog.model.BaseResponse;

/**
 * @author Vic.xu
 *
 */
@RestController
@RequestMapping("/log")
public class LogController {

	
	@Resource
	private LogService logService;
	
	private Logger logger = LoggerFactory.getLogger(LogController.class);
	
	/**
	 * 配置的文件夹列表
	 * @return
	 */
	@RequestMapping("/dir")
	public BaseResponse<?> dir(HttpServletRequest request){
		Set<String> data = logService.dirSet();
		if (data.isEmpty()) {
			return BaseResponse.error("尚未配置日志文件夹");
		}
		return BaseResponse.success(data).setMsg(wsUrl(request));
	} 
	//socket url like:  ws://localhost:18092/log/msg   wss://www.localhost:18092/log/msg  and so on
	private String wsUrl(HttpServletRequest request) {
		String serverName = request.getServerName();
		String contextPath = request.getContextPath();
		int serverPort = request.getServerPort();
		String ws = "wss://";
		
		String scheme = request.getScheme();
		if("http".equalsIgnoreCase(scheme)) {
			ws = "ws://";
		}
		String url = ws  + serverName  + ":" + serverPort + "/" + contextPath + ReadlogConstant.SOCKET_URL;;
		logger.info("scheme:[{}];serverName:[{}];serverPort:[{}];contextPath[{}]", new Object[] {scheme, serverName, serverPort, contextPath});
		return url;
	}
	
	/**
	 * 某个文件夹下的日志文件
	 * @return
	 */
	@RequestMapping("/logs")
	public BaseResponse<?> logs(String dir){
		if(StringUtils.isEmpty(dir)) {
			return BaseResponse.error("请选择某个日志文件夹");
		}
		
		if(!logService.dirSet().contains(dir)){
			return BaseResponse.error("不合法的日志文件夹");
		}
		List<LogModel> logs = logService.logs(dir);
		return BaseResponse.success(logs);
	}
	
	/**
	 * 下载文件
	 * @param dir
	 * @param fileName
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping("/download")
	public void download(String dir, String fileName, HttpServletResponse response) throws IOException {
		
		if(StringUtils.isEmpty(dir) || !logService.dirSet().contains(dir)){
			write(response, "不合法的日志文件夹");
			return;
		}
		File file = new File(dir, fileName);
		if(!file.exists()) {
			throw new IOException("不存在的路径");
		}
//		response.setContentType("application/force-download");
		response.setContentType("application/octet-stream");
		response.setHeader("Content-disposition", "attachment; filename=\"" + file.getName() + "\"");
		FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
	}
	
	
	private void write(HttpServletResponse response, String msg) {
		response.setHeader("content-type", "text/html;charset=UTF-8");
		try {
			response.getWriter().write(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
