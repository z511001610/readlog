/**
 * 
 */
package pers.vic.readlog.log.util;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.RandomAccessFile;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pers.vic.readlog.constant.ReadlogConstant;
import pers.vic.readlog.log.model.LineAndPosition;

/**
 * @author Vic.xu
 *
 */
public class LogUtils {

	private static Logger logger = LoggerFactory.getLogger(LogUtils.class);

	/**
	 * 获取文件的总行数
	 * 
	 * @param file
	 * @return
	 */
	public static int getFileCountLineNumber(File file) {
		if (file.length() <= 0) {
			return 0;
		}
		try (LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file))) {
			lineNumberReader.skip(Integer.MAX_VALUE);
			return lineNumberReader.getLineNumber() + 1;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return 0;
	}
	
	/**
	 * 获取文件指定行的指针
	 * @param lastLineCount 倒数读取多少行
	 * @return
	 */
	private static Long getLinePosition(File file, int lastLineCount) {
		long len = file.length();
		int line = 0; // 当前是倒数第多少行
		long pos = len;// 指针位置
		try (RandomAccessFile acf = new RandomAccessFile(file, "r")) {
			loop: for (; pos > -1;) {
				--pos;
				acf.seek(pos);
				int readByte = acf.readByte();
				switch (readByte) {
				case '\n':
				case '\r':
					line++;
					if (line >= lastLineCount) { // 判断是否是我们需要读取的倒数第lines 行
						break loop;
					}
				default:
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pos;
	}

	/**
	 * 获得开始的行数和位置
	 */
	public static LineAndPosition calcPosition(File file) {
		long position = 0L;
		int line = 0;
		int count = getFileCountLineNumber(file);
		if(count > ReadlogConstant.DEFAULT_READ_LINE) {
			line = count - ReadlogConstant.DEFAULT_READ_LINE;
			position = getLinePosition(file, ReadlogConstant.DEFAULT_READ_LINE);
		}
		return new LineAndPosition(line, position);
	}
	
	public static String redLog(File file, LineAndPosition lineAndPosition) {
		long position = lineAndPosition.getPosition();
		int line = lineAndPosition.getLine();
		try (RandomAccessFile acf = new RandomAccessFile(file, "r")){
			//如果文件没有新增信息
			if(acf.length()<= position) {
				return "";
			}
			acf.seek(position);
			StringBuffer data = new StringBuffer();
			String curLine = null;
			while((curLine = acf.readLine()) != null) {
				line++;
				curLine = new String(curLine.getBytes("ISO-8859-1"),"utf-8");
				data.append(formatLine(line, curLine));
				
				position = acf.getFilePointer();
			}
			//更新行数和位置
			lineAndPosition.setLine(line);
			lineAndPosition.setPosition(position);
			return data.toString();
		}catch (Exception e) {
			logger.error("redLog has error ", e);
		}
		return "";
	}
	
		private static String formatLine(Integer line, String data) {
			if(StringUtils.isBlank(data)) {
				return "";
			}
		return new StringBuilder("<span class=\"line\">[").append(line).append("]</span>")
				.append(data).append("<br />").toString();
	}

}
