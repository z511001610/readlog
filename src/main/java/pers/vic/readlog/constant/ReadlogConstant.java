package pers.vic.readlog.constant;

public final class ReadlogConstant {
	
	/**
	 * 允许的后缀 .log
	 */
	public static final String ENABLE_SUFFIX_LOG = ".log";
	
	/**
	 * 允许的后缀  .log
	 */
	public static final String ENABLE_SUFFIX_TXT = ".txt";
	
	/**
	 * 一次性默认读取多少行
	 */
	public static final Integer DEFAULT_READ_LINE = 30;
	
	/**
	 * socket  地址  ws://localhost:18092/log/msg
	 */
	public static final String SOCKET_URL = "/log/msg";
	

}
