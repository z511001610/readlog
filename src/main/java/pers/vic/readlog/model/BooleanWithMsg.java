package pers.vic.readlog.model;

import java.io.Serializable;

/**
 * 带备注的boolean类型
 *
 */
public class BooleanWithMsg implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean success;
	
	private String message;
	
	private int msgCode = 0;


	
    public BooleanWithMsg() {
		super();
	}
    
    public static BooleanWithMsg True(){
		return new BooleanWithMsg(Boolean.TRUE, "");
	}
	public static BooleanWithMsg True(String message){
		return new BooleanWithMsg(Boolean.TRUE, message);
	}
	public static BooleanWithMsg False(String message){
		return new BooleanWithMsg(Boolean.FALSE, message);
	}

	public static BooleanWithMsg False(String message, int msgCode) {
		return new BooleanWithMsg(Boolean.FALSE, message, msgCode);
	}
	private BooleanWithMsg(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}

	private BooleanWithMsg(boolean success, String message, int msgCode) {
		this(success, message);
		this.msgCode = msgCode;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

	@Override
	public String toString() {
		return "BooleanWithMsg [success=" + success + ", message=" + message + ", msgCode=" + msgCode + "]";
	}
    
    
}
