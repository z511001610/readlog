package pers.vic.readlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReadlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadlogApplication.class, args);
	}
	
}
