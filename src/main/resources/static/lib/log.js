
$(function() {
	readDirect();
	buttonClickOperation();
	openTableEventBind();
});
var open = true;
function openTableEventBind() {
	$("button.open").on("click", function() {
		var $btn = $(this);
		$("table").toggle();
		open = !open;
		$btn.text(open ? "收起表格" : "展开表格");
	});
}

function readDirect() {
	var url = ctx + "/log/dir";
	$.get(url, function(result) {
		if (result.code != 0) {
			alert(result.msg);
			return;
		}
		console.info("console  ws  address is :" + result.msg);
		var html = '';
		result.data.forEach(function(item, idnex, array) {
			html += '<option value="' + item + '">' + item + '</option>';
		});
		$("#dir").html(html).trigger("change");
	});
}

var templateTr =
	`<tr>
		<td	class="d1"></td>
		<td class="d2"></td>
		<td class="d3"></td>
		<td class="d4"></td>
		<td class="d5">
			
			<button type="button" class="btn btn-xs btn-default tail"  data-toggle="tooltip" title="查看日志" >
				查看<i class="glyphicon glyphicon-eye-open"></i>
			</button>
			
			<button type="button" class="btn btn-xs btn-info download"  data-toggle="tooltip" title="下载日志">
				下载<i class="glyphicon glyphicon-download"></i>
			</button>
		</td>
	</tr>`;
function changeDir() {

	var val = $("#dir").val();
	if (!val) {
		retrun;
	}

	var url = ctx + "/log/logs";
	$.post(url, { dir: val }, function(result) {
		if (result.code != 0) {
			alert(result.msg);
			return;
		}
		var $tr = $(templateTr);
		var $tbody = $("table.table tbody");
		$tbody.html("");
		result.data.forEach(function(item, index, array) {
			var $curTr = $tr.clone(true);
			$(".d1", $curTr).html(index + 1);
			$(".d2", $curTr).html(item.fileName);
			$(".d3", $curTr).html(item.sizeDesc);
			$(".d4", $curTr).html(item.updateDate);
			$tbody.append($curTr);
		});
	});
}

var websocket = null;
function buttonClickOperation() {
	$(document).on("click", "button.tail", function() {
		var dir = $("#dir").val();
		var fileName = $(this).closest("tr").find("td.d2").text();
		//判断当前浏览器是否支持WebSocket, 主要此处要更换为自己的地址
		if ('WebSocket' in window) {
			if (websocket != null) {
				websocket.close();
			}
			
			$("button.open").trigger("click");
			$("#detail").text(fileName);
			cleanMsg();
			console.info("ws:" + ws);
			websocket = new WebSocket(ws);
			bindEvent(dir + ";" + fileName);
		} else {
			alert('Not support websocket')
		}

	});

	$(document).on("click", "button.download", function() {
		$(this).prop("disable", true);
		var dir = $("#dir").val();
		var fileName = $(this).closest("tr").find("td.d2").text();
		var href = ctx + "/log/download?dir=" + dir + "&fileName=" + fileName;
		var $a = $('<a href="' + href + '"></a>');
		$("body").append($a);
		$a[0].click();
		$a.remove();
		$(this).prop("disable", false);

	});
}


function bindEvent(message) {
	//连接发生错误的回调方法
	websocket.onerror = function() {
		setMessageInnerHTML("error");
	};

	//连接成功建立的回调方法
	websocket.onopen = function(event) {
		setMessageInnerHTML("<h3>open socket!</h3>");
		send(message);
	}

	//接收到消息的回调方法
	websocket.onmessage = function(event) {
		setMessageInnerHTML(event.data);
	}

	//连接关闭的回调方法
	websocket.onclose = function() {
		setMessageInnerHTML("<h3>closed</h3>");
	}

	//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
	window.onbeforeunload = function() {
		websocket.close();
	}
}


//将消息显示在网页上
function setMessageInnerHTML(innerHTML) {
	$("#message").append(innerHTML);
	$("#filelog-container").scrollTop($("#filelog-container div").height() - $("#filelog-container").height());
}

//关闭连接
function closeWebSocket() {
	if(!websocket){
		return;
	}
	websocket.close();
}

//发送消息
function send(message) {
	websocket.send(message);
}

function  cleanMsg(){
	$("#message").html("");
}






